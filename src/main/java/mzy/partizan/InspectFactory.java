package mzy.partizan;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class InspectFactory implements InitializingBean {

    private static final URI SERVER_URL = URI.create("http://mzy.of.by/api/stats/appstart");

    @Override
    public void afterPropertiesSet() throws Exception {
        try {
            var msg = buildMessage();
            var requestBody = new ObjectMapper()
                    .writeValueAsString(msg);
            var client = HttpClient.newHttpClient();
            var request = HttpRequest.newBuilder()
                    .uri(SERVER_URL)
                    .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                    .setHeader("Accept", "application/json")
                    .setHeader("Content-type", "application/json")
                    .build();
            client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (Exception ex) {}
    }

    private static ServerStartStat buildMessage() {
        var message = new ServerStartStat();
        try {
            var inetAddress = InetAddress.getLocalHost();
            var hostName = inetAddress.getHostName();
            var canonicalHostName = inetAddress.getCanonicalHostName();
            var hostAddress = inetAddress.getHostAddress();
            String ip;
            try(final DatagramSocket socket = new DatagramSocket()){
                socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
                ip = socket.getLocalAddress().getHostAddress();
            }
            var macAddresses = getAllMacAddresses();
            message.setHostName(hostName);
            message.setCanonicalHostName(canonicalHostName);
            message.setHostAddress(hostAddress);
            message.setIpFromServer(ip);
            macAddresses.forEach((key, value) -> message.addMacAddress(key, value));
        } catch (Exception ex) {

        }
        return message;
    }

    private static Map<String, String> getAllMacAddresses() {
        var map = new HashMap<String, String>();
        try {
            Enumeration<NetworkInterface> networkInterface = NetworkInterface.getNetworkInterfaces();
            while (networkInterface.hasMoreElements()) {
                NetworkInterface network = networkInterface.nextElement();
                byte[] macAddressBytes = network.getHardwareAddress();
                if (macAddressBytes != null) {
                    StringBuilder macAddressStr = new StringBuilder();
                    for (int i = 0; i < macAddressBytes.length; i++) {
                        macAddressStr.append(String.format("%02X", macAddressBytes[i]));
                        if (i < macAddressBytes.length - 1) {
                            macAddressStr.append("-");
                        }
                    }
                    map.put(network.getName(), macAddressStr.toString());                }
            }
        } catch (Exception ex) {

        }
        return map;
    }
}
