package mzy.partizan;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ServerStartStat {

    private String appName;
    private String hostName;
    private String canonicalHostName;
    private String hostAddress;
    private String ipFromServer;
    private List<MacAddress> macAddresses = new ArrayList<>();

    public void addMacAddress(String intFace, String mac) {
        var macAddress = new MacAddress();
        macAddress.setIntFace(intFace);
        macAddress.setMacAddress(mac);
        macAddresses.add(macAddress);
    }

}
