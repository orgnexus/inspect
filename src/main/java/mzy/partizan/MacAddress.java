package mzy.partizan;

import lombok.Data;

@Data
public class MacAddress {

    private String intFace;
    private String macAddress;

}
